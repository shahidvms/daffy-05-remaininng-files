Rick L. Shefka
VP Operations >> CEO >> Vice PresidentClermont, FL
252.560.3156
rickshefka@yahoo.com www.linkedin.com/in/rickshefka/
Contract Logistics & Supply Chain Solution Sales - Trusted Executive Leader - Building Performance-Driven Culture
GLOBAL SUPPLY CHAIN MANAGEMENT - OPERATIONS - COST REDUCTION STRATEGY - CUSTOMER SUCCESS
* Synergistic growth driver and business executive with 12 years of experience in construction, logistics and operations for a carrier, freight forwarder/NVO and/or consolidator/third--Party Logistics (3PL) organization. Combine construction background with business acumen.
* Adaptable leader of a national distributed workforce, known to deliver strong financial results (even during pandemic) while galvanizing supply chain, sales, customer services, channel distribution and logistics operations. Able to walk into any industry and achieve success.
* Established competitive vendor network internationally. Identified and developed relationships with multiple manufacturers and distributors in the U.S. and Europe. Decreased cost for mobile workstation buildouts by 65%. Decreased recurring equipment and material costs by 40%.
* Forward-thinking change agent praised for leading multi-year transformational efforts on several aspects of operations, supply chain, and logistics.
* Demonstrated planning, project management and analytical skills; able to partner and communicate with all levels of an organization from hourly associates to senior management, as well as with customer contacts.

EXECUTIVE CORE COMPETENCIES
- Cross-functional Team Leadership- P&L Management/Budgeting 
- Program Leadership- Operational Effectiveness - Process & Service Excellence
- Board Engagements & Collaboration- Business Analysis - Customer Success Strategies
- Supply Chain Operations & Distribution - Acquisitions & Integration- Operations Leadership
- Vendor Relationship Management- Customer Service Management- Revenue & Profit Growth
Executive Career History & Highlights

VICE PRESIDENT OF OPERATIONS 
Advanced Plumbing Technology - Clermont, FL | 2018 - 2021
$15M regional provider of pipe inspections, residential, commercial, and municipal pipelining services.
Team: 10 Direct & 70 Total | Direct P&L: $15M | Reported to: CEO | Operational Transformation
Recruited to downsize and restructure the company during period of change. Championed lean operations and continuous improvement to reduce overhead spend while launching multimillion-dollar revenue-generating departments across residential services and government contracts. Restructured multiple departments to maximize capabilities and increase production.
* Decreased material and equipment costs by 40-60% after building international supply chain.
* Built $5M revenue-generating residential work department projected to deliver up to $10M in revenue by 2022.
* Launched $8M municipal work department with 2022 revenue projections of $15-20M.
* Reduced outsourced work by 40% after insourcing services by leveraging existing staff and capabilities.
* Grew company with multiple streams of revenue to avoid layoffs due to COVID-19 and decreased demand.
* Improved internal culture, employee morale, and job satisfaction by promoting an employee-centric culture.
* Restructured 3 departments by focusing employees on specific and repetitive tasks to increase efficiency while launching training programs to upskill staff.

CEO - OWNER
Dynamic Transport Solutions - Kinston, NC & Clermont, FL | 2012 - 2020
Logistics and Supply Chain Management providing full-scale solutions for LTL and FTL shipments in the US and Canada.
Team: 3 Direct | $4M P&L | Supply Chain & Logistics | People Development
Launched supply chain and logistics company from the ground up, growing to over $4M in annual revenue. Clients included multibillion-dollar companies, Commodity Traders and Glencore Trafigura, and smaller freight, steel, produce, chemical and automotive parts companies. Shipped from the U.S., Canada, and Mexico. Developed business by targeting high-growth regions across TX, FL, AZ, AL, MS, GA, CA, TN, IN, KT, and diverse industries, including areas with lower carrier capacities. 
* Grew business from $200K in 2012 to $4.3M by 2019.
* Developed 10 performance-driven account representatives.
* Leveraged existing carrier network and strong relationships to acquire new customers by overcoming existing capacity and customer service issues with previous vendors. 
* Ensured 15K+ shipments were picked up and delivered 99% on time, including 300 for a single customer after leading 3-month project to build new shipping network between pickup and delivery location, negotiating shipping prices, and increasing front-haul costs while reducing back-haul costs.

LOGISTICS ACCOUNT EXECUTIVE
Total Quality Logistics - Cincinnati, OH | 2011 - 2012
A $3.6B national freight brokerage firm, recognized as one of the largest in the US.
Reported to: Group Sales Manager | Supply Chain & Logistics | Customer Service | Sales
Recruited to drive business growth by generating leads, building relationships, and producing sales with potential and current customers while delivering superior customer service as first customer point of contact. Blended sales, customer service, and strategy together for the purpose of managing transportation and shipments for large client portfolio.
* Delivered $2M in new revenue within 8 months as consultative customer advisor and point of contact.
* Maintained 99% on-time service, partnering with multiple industries to deliver on supply chain needs.
* Saved $2K for key customer after consolidating multiple partial shipments into 3 truckloads, ensuring delivery of all parts in less than 3 days, leveraging market conditions, area availability, and product volatility.
* Ranked #1 in sales and customer acquisition in hiring class of 50.
* Trained and developed 2 successful Account Executives.

MANAGING PARTNER
S and W All Natural Distribution, LLC - Clermont, FL | 2009 - 2010
A $500K wholesale supplement and vitamin company with customers in 3 US states.
Team: 3 Direct | $500K P&L | Supply Chain & Logistics | Training & Development
Launched supplement and vitamin wholesale company specializing in all-natural vitamins and supplements. Shipped products to small and medium B2C beauty and fitness retailers across FL, GA, and NC Built and trained a performance-driven sales team and oversaw supply chain and inventory operations. 
* Secured 50 customers and $500K in annual revenue.
* Developed multi-channel marketing strategy and trained 3 sales representatives.

Education 

BACHELOR OF SCIENCE COURSEWORK - BUSINESS ADMINISTRATION, LOGISTICS & SUPPLY CHAIN
East Carolina University, Greenville, NC | GPA: 4.0/4.0
        



