
Lakeya Reel

Lancaster, CA 93536
lakeya.martin35@gmail.com

Summary

Focused healthcare professional well-versed in promoting a healthy lifestyle by coordinating and supervising activities in alignment with client needs and abilities. Proven history of inspiring and fostering client well-being across 11 years of experience in related roles. Dedicated to serving unique needs of each client and improving quality of life.
Patient and calm Direct Care Worker with in-depth understanding of medication administration and medical equipment operations. Assists with activities of daily living, preparing meals and housekeeping. Committed to top-notch care for disabled, impaired and chronically ill individuals.
Proactive Personal Care Assistant eager to apply aptitude for building client rapport through compassionate care and communication. Dedicated to resolving client challenges by creating foundation of healthy habits to increase independence and comfort. Committed to progressing clients' journeys to optimal treatment outcomes.
Dedicated Direct Care Worker passionate about helping clients lead fulfilling lives by managing health-related conditions. Knowledgeable about medication administration, appointments and social activities. Well-organized and compassionate with good relationship-building skills.
Compassionate Direct Care Worker with CBRF certification. Accommodating and respectful professional with hands-on experience providing companionship and emotional support. Proficiently documents vital statistics and health metrics.
Committed Personal Care Assistant well-versed in assisting and supervising clients in daily activities, including meal preparation and basic housekeeping. Compassionate and patient with strong background in client care. Ready to undertake challenging cases and provide superlative health outcomes.
Observant personal care assistant with successful background delivering heartfelt care to residents. Certified in cpr. Compassionate and service-oriented individual well-educated in medication administration and schedule management.
Detail-oriented Direct Care Worker supports daily living activities and provides transportation to enhance client well-being. Seasoned care expert experienced with developmentally disabled clients. Committed to promoting comfort with dignity and compassion.
Kind Caregiver with experience addressing physical and emotional needs. Familiar with scheduling and managing appointments, coordinating household support and offering caring companionship. Well-organized with medications and physically fit to handle any client need.
Outgoing Direct Care Worker experienced with patients with disabilities and acute ailments. Well-versed in assisting patients with daily living tasks. Committed to improving patient well-being through personal care and interactive engagement.
Reliable personal care professional bringing 11 years of experience in Personal Care Assistant roles. Accustomed to addressing physical and mental disabilities and monitoring needs for convalescent patients. Trusted to care for patients at varying stages of life.

Skills

Compassionate patient care
Medication management
Adaptable under pressure
Behavior redirection
Client relationship management
Meal planning
Household organization
Tact and diplomacy
First-aid and CPR
Community integration
First aid and safety

Client documentation
Community activities
Quality program protocols
Case management experience
Compassionate client care
Case management
Care plan management
Knowledge of state regulations
Records maintenance
Records management

Experience

February 2009 to August 2021
Absolute Care Management Jonesboro , AR
Personal Care Assistant    

Made beds, swept floors and sanitized surfaces to support activities of daily living.
Maintained daily living standards by assisting clients with personal hygiene needs.
Provided ongoing compassionate patient care for each client.
Planned and prepared nutritious meals and snacks to meet diabetic, low sodium and high protein diets.
Laundered clothing and bedding to prevent infection.
Reported concerns to nurse supervisor to promote optimal care.
Liaised with supervisor to review cases and improve care.
Directed patients in passive and active ROM exercises to maintain musculoskeletal functions and increase strength.
Coordinated doctor appointments, exercise, recreation and family visits to maintain schedule.
Completed scheduled patient check-ins and progress reports for clients.
Developed patient care plans with doctors and registered nurses.
Maintained network of connected caregivers to promote continuous professional development.
Maintained clean and well-organized environment for client happiness and safety.
Improved patient outlook and daily living through compassionate care.
Observed patients for changes in physical, emotional, mental or behavioral condition and injuries.
Created safe and positive living situations for clients by communicating with family and other staff about concerns or challenges.
Followed care plan and directions to administer medications.
Monitored vital signs and medication use, documenting variances and concerning responses.
Aided with mobility and independence for disabled individuals and continually monitored safety.
Assisted with meal planning to meet nutritional plans.
Examined and addressed lacerations, contusions and physical symptoms to assess and prioritize need for further attention.

April 2003 to August 2005
Burns Security Services Jonesboro , AR
Security Officer    

Guarded restricted areas to prevent unauthorized entry.
Patrolled and secured industrial and commercial premises to prevent intrusion.
Followed established security and safety procedures and posted orders to include enforcement of company rules, policies and regulations.
Secured personnel and premises by inspecting buildings, patrolling property and monitoring surveillance cameras.
Documented security-related situations and submitted in-depth reports to superiors.
Enforced security regulations and escorted non-compliant individuals to private areas for processing.
Responded to alarms and disturbances to maintain safety.
Checked footage and live feeds from surveillance cameras for trespassers and criminal activity.
Warned persons of rule infractions or violations and evicted violators from premises.
Inspected parking lot to verify parking permits and ticketed or towed unauthorized vehicles.
Monitored premises and recorded activity in daily officer reports.
Searched individuals and baggage for devices, weapons and other prohibited items.
Verified integrity and accuracy of photo ID's, tickets and passports prior to authorizing passage.
Monitored security cameras and fire, building and alarm systems.
Triaged problems quickly and provided precise and clear information while working under minimum supervision.
Circulated among patrons and customers to keep abreast of emergent security situations.
Acted as first-responder for medical emergencies, incoming calls and code red situations prior to arrival of paramedics and law enforcement.
Operated scanning equipment to keep entrance lines moving efficiently.
Worked with local and federal law enforcement agents to apprehend suspicious individuals.
Immediately reported safety hazards for remediation response.

May 1998 to September 2000
Stevens Square Community Organization Lepanto , AR
Secretary     

Maintained organized filing system of paper and electronic documents.
Coordinated communications, including taking calls, responding to emails and interfacing with clients.
Completed supply orders and maintained appropriate levels of office supplies.
Created agendas, meeting notes and other documents to enhance collaborative process.
Established clear and consistent administrative procedures to minimize errors and avoidable delays.
Prepared packages for shipment by generating packing slips and setting up courier deliveries.
Maintained office safety by screening visitors, updating logs and issuing temporary passes.
Produced and distributed memos, newsletters, email updates and other forms of communication.
Scheduled conferences and associated travel arrangements, including hotel, airfare and ground transportation.
Verified operation of office equipment by completing preventive maintenance requirements and calling for repairs.
Fostered productivity by coordinating itinerary and scheduling appointments.
Assisted with answering phones, filing paperwork, entering data and [Task] to support operations department.
Provided administrative support to marketing team members, fostering timely project completion.
Performed tasks to aid in research projects including collecting and entering data and assisting with analyzing data and preparing reports and manuscripts.
Supported team members by restocking supplies, maintaining office equipment and [Action].

Education and Training

Concord Career College Memphis , TN
Some College (No Degree) Massage Therapy  
