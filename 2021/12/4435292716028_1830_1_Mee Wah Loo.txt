Mee Wah Loo, BSN, RN-BC
Passionate multilingual registered nurse with almost 9 years of experience in acute and subacute clinical settings.
Phone:
1-917-612-8396
Address:
201 W 72nd St
Apt 20E
New York NY 10023
Email:
jloonyc@yahoo.com

Awarded the medical-surgical certification by American Nurses Credentialing Center (ANCC) on March 9th, 2019. Since obtaining her Registered Nursing license, she has since gone on to gain five years of full-time experience as a telemetry/ medical-surgical nurse. Currently acts as a unit-based council chairperson to implement positive change to improve patient-care experience and outcomes and reduce nurse burnout. Possess a prior business management background. A natural leader who is growth-minded and is a positive change agent at work and in life. Experienced in the areas of women's health oncology, OB/GYN, medical-surgical telemetry and geriatric care. Respected as a member of the multidisciplinary team for her commitment to providing quality nursing care.

Experience

Mount Sinai West
August 2018 - present
Staff Nurse
Full-time telemetry unit nurse and Unit-based council chairperson.

Weill Cornell Medical College
September 2017 to Present
Agency Staff Nurse
Per Diem work at the Myeloma Center where I start intravenous access for patient receiving chemotherapy. Assess patient's reaction and tolerance to chemotherapy infusion. Verify and check chemo drugs with oncology nurses.

NYU Langone
September 2017 to Present
Agency Staff Nurse
Per Diem work at NYU. Answering pager calls from various specialty clinics to transport pt to ER for admission. Administer injections, take vital signs, assess patients and provide patient teaching.

Montefiore Medical Center
June 2015 to Present
Registered Nurse
Medical Surgical/ Telemetry unit nurse in Moses Hospital in Bronx, NY & charge nurse at Montefiore GYN oncology clinic, a Montefiore Women's Outreach Program in Flushing, NY.

Montefiore Medical Center
April 2014 to June 2015
Licensed Practical Nurse
Assisted gynecologic oncologists with diagnostic procedures in a clinic settings at Eastchester Park Center in Bronx, NY. Nurse in charge at the Women's Outreach Program in Flushing, NY.

Sprain Brook Manor, Yonkers, NY
December 2012 to March 2014
Licensed Practical Nurse
Involved in patient care and nursing duties such as assisting the patients in dosage of medicines, wound care and providing guidance on healthcare for them.
Assigned duties and overlooked the duties of other nurses and CNAs (average of 5-6 per shift) during shifts. Orienting new nurses to the facility and serve as a source of information about the administrative duties to the new nurses.
Maintaining optimal standard of care within nursing scope of practice and facility policies.

Education

NYU Rory Meyers College of Nursing
September 2019 to September 2022
Masters in Science
Currently studying part-time for her nurse practitioner license with major in adult gerontology primary care.

Lehman College
July 2015 to July 2017
Bachelor of Science in Nursing

CUNY LaGuardia Community College
December 2013 to December 2014
Associate in Applied Science Degree in Nursing
Medical Surgical and ER Rotations at Jamaica Hospital, Queens, NY 9/14-12/14
Maternity and Pediatrics and ER Rotations at Elmhurst Hospital Center, Elmhurst, NY 2/14-3/14

CUNY LaGuardia Community College
September 2009 to June 2012
Licensed Practical Nursing Program
CLINICAL ROTATIONS
3/12 - 6/12 Medical Surgical Unit Elmhurst Hospital Center, Elmhurst, NY
2/12 - 4/12 Mental Health Unit Elmhurst Hospital Center, Elmhurst, NY
1/12 Pediatric Unit Elmhurst Hospital Center, Elmhurst, NY
2/12 Maternal Unit Elmhurst Hospital Center, Elmhurst, NY
9/11-12/11 Long Term Care Parker Jewish Institute, Long Island, NY

Nanyang Polytechnic, Singapore
January 2005 to March 2008
Diploma in Business Management

Skills

Basic Life Support Certification - American Heart Association, New York, Proficient in Microsoft Excel, Word, PowerPoint, and Windows XP/7/8 and Internet Research.

Projects

RN student Capstone Profile

https://lagcc-cuny.digication.com/mloo_capstone/Introduction/

Professional Membership

Member, American Academy on Communication in Healthcare (AACH)
January 2015 to Present
Improving communication in healthcare
Improving communication in healthcare between providers and patients, thus improving patient outcome.

Languages

English

(Native or bilingual proficiency)

Mandarin Chinese

(Native or bilingual proficiency)

Cantonese

(Native or bilingual proficiency)

Spanish

(Limited working proficiency)

Scholarships

Interprofessional scholarship - AACH s ENRICH Course
June 18-21, 2015
Drexel University, Philadelphia
Recipient of the Interprofessional scholarship to attend AACH's ENRICH Course: Meeting at the Crossroads of Communication and Professionalism. Funding was provided by a generous donor to encourage interprofessionalism in medicine.

Dean's List Spring 2017
Mar-June 2017
Lehman College

-----END OF RESUME-----

Resume Title: Healthcare - Nursing - RN

Name: Mee Wah Loo
Email: jloonyc@yahoo.com
Phone: true
Location: New York-NY-United States-10023

Work History

Job Title: Staff Nurse 08/01/2018 - Present
Company Name: Mount Sinai West

Job Title: Agency Staff Nurse 09/01/2017 - Present
Company Name: Weill Cornell Medical College

Job Title: Agency Staff Nurse 09/01/2017 - Present
Company Name: NYU Langone

Job Title: Registered Nurse 06/01/2015 - Present
Company Name: Montefiore Medical Center; Moses Hospital

Job Title: Licensed Practical Nurse 04/01/2014 - 06/01/2015
Company Name: Montefiore Medical Center

Job Title: Licensed Practical Nurse 12/01/2012 - 03/31/2014
Company Name: Sprain Brook Manor

Education

School: NYU Rory Meyers College of Nursing, Major:
Degree: Master's Degree, Graduation Date:

School: Lehman College, Major:
Degree: Bachelor's Degree, Graduation Date:

School: CUNY LaGuardia Community College, Major:
Degree: Bachelor's Degree, Graduation Date:

Additional Skills And Qualifications

Most Recent Job Title: true
Most Recent Pay: USD per
Managed Others: true
Languages Spoken: English
Work Authorization: Not Specified
Security Clearance: No
Felony Conviction: No

Desired Pay: USD per
Desired Job Types: Full Time
Travel Preference:

Currently Employed: No
Jobs Last Three Years: 0
Last Job Tenure Months: 0
Highest Degree: 4 Year Degree
Certifications:
Employment Type:
Military Experience:
Last Updated: 12/1/2021 12:00:00 AM

Desired Shift Preferences:

Interests:
Nurse

Downloaded Resume from CareerBuilder
RD90X76Q3305CWCDY6W


