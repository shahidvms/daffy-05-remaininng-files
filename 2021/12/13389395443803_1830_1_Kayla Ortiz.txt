Kayla Ortiz

Spring, TX
kaylabeth777@gmail.com
8323140986
Willing to relocate: Anywhere
Authorized to work in the US for any employer

Work Experience
Receptionist

H&R Block - Spring, TX
January 2021 to Present

Greet clients in a personalized, friendly, and inviting manner
Match clients with the best-suited tax professional for their needs
Schedule clients how they would like to be scheduled
Handle client exits by ensuring all current and future needs are met
Maintain office cleanliness and organization of resources with team members
Other duties as assigned
Sales and/or marketing experience
Ability to multi-task
Strong organizational and time-management skills
Knowledge of cash register operations
Knowledge and experience with a Windows based computer system

Server/Cashier

Bubba T - Willis, TX
September 2020 to Present
As a server I took orders, answered questions about the menu and food, sell the restaurant's food
and drinks, took payment, communicated orders with the kitchen staff, seated customers, helped with
customer service and cleaning.

Server

Uncle Julio's Mexican From Scratch - Spring, TX
March 2020 to July 2020
As a server I took orders, answered questions about the menu and food, sell the restaurant's food
and drinks, took payment, communicated orders with the kitchen staff, seated customers, helped with
customer service and cleaning.

Education
Associate's in Business

Lone Star College System - The Woodlands, TX
August 2020 to Present

�High school diploma

Deer Park High School - Deer Park, TX
August 2014 to May 2018

Skills
Customer service
Computer skills
Front Desk
Serving Experience

Certifications and Licenses
Food Handler Certification

�

-----END OF RESUME-----

Resume Title: Healthcare - Nursing - RN

Name: Kayla Ortiz
Email: kaylabeth777@gmail.com
Phone: true
Location: Spring-TX-United States-77383

Work History

Job Title: Receptionist 01/01/2021 - Present
Company Name: H&R Block

Job Title: Server/Cashier 09/01/2020 - Present
Company Name: Bubba T

Job Title: Server 03/01/2020 - 07/31/2020
Company Name: Uncle Julio's Mexican From Scratch

Education

School: Lone Star College System, Major:
Degree: Bachelor's Degree, Graduation Date:

School: true, Major:
Degree: High School, Graduation Date:

School: Deer Park High School, Major: Not Applicable
Degree: High School, Graduation Date:

Additional Skills And Qualifications

Most Recent Job Title: true
Most Recent Pay: USD per
Managed Others: true
Languages Spoken: English
Work Authorization: Not Specified
Security Clearance: No
Felony Conviction: No

Desired Pay: USD per
Desired Job Types: Full Time
Travel Preference:

Currently Employed: No
Jobs Last Three Years: 0
Last Job Tenure Months: 0
Highest Degree: High School Degree
Certifications:
Employment Type:
Military Experience:
Last Updated: 10/17/2021 12:00:00 AM

Desired Shift Preferences:

Interests:
Restaurant - Food Service
Tourism / Restaurant-Food Service / Security

Downloaded Resume from CareerBuilder
R2R5SS71MKYFNK4K9L4


