Braylon Oliver

Lafayette, LA 70507
braylondoliver@gmail.com
3374536600
Customer service certified
CPR certified
High school graduate
Authorized to work in the US for any employer

Add your headline
Work Experience

Sales Associate

Kirkland's - Lafayette, LA
October 2020 to Present
Make sales to customers
Reorganize the store
Unload truck
Answer phone calls
Crew Member

Tropical Smoothie Cafe - Lafayette, LA
July 2020 to Present
Help prep for the busy day
Take orders
Answer and take calls
Take online orders
Restock for next shifts
Crew Member

Tropical Smoothie Cafe - Lafayette, LA
July 2020 to September 2020
Worked well in a fast-paced, high pressure environment
Greeted and served customers
Answered customer questions
Answered and made phone calls
Maintained a positive relationship with coworkers
Restocked supplies
Prepared food and stocked the freezer
Took cash and credit card payments
Provided customer service with a smile
Education

High school or equivalent in Nursing

Northside High School - Lafayette, LA
August 2019 to May 2020