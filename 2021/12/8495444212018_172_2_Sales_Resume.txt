
Objective: To serve the customer in the best way possible, exchange knowledge and create a secure future for myself within the company.


Summary
Immense sales experience from my previous position at IGS Energy and current sales/customer service job experience at One Support and top sales on my team at TWC Inc within the tech branch.
Over 6 years of combined sales experience, also 8+ years of customer service.
Extremely versatile, can fit into any position and exceed any goal set forth.
Completed my Associates degree in User Support.
Education

Lakeland Community College, Kirtland OH, May 2017.
Associate of Applied Business, in IT with a User Support Specialist degree. Courses: Managing and Optimizing PC's, Help Desk Concepts and Management. Along with Certifications earned from Lakeland Community College, IT Professional Cert and IT Specialist Certificate.
Technical Knowledge

Software Applications: Microsoft Office 2003-365.
Programming Languages: SQL, Java.
Operating Systems: Windows XP, Windows Vista, Windows 7/8, Windows 10 and working knowledge of Mac.
Additional Knowledge: Cold sales tactics. Extensive knowledge of cable systems as an I/R Technician/ ISP Tech/Sales Rep.

Work Experience
Customer Service/Tech Support/Sales Supervisor, One Support Inc., San Marcos, TX. July 2019 - Current. 
Exceeded I.T metrics when taking incoming calls.
Surpassed all sales metrics while building a new contract from the ground up as a sales supervisor.
Strengthened the ability to think on my toes, and go with the flow on a daily basis, even with extreme changes and pressure from upper management. 
Many customer kudos for positive experiences, countless raises and position changes with upward mobility achieved through immense focus and ability. 


Territory Sales Manager IGS Inc. Jun 2018 - Jul 2019
Create new customer relationships from cold knocking my assigned territory.
Follow all solicitation rules/regulations and local laws while conducting business for IGS.
Follow up with customers with appointments, phone calls, and even cold visits. 
Exceeded all sales goals put forth since starting, promoted to Territory Manager in my first four weeks.

Sales/Pricing/Shipping Receiving Macy's/Shipping/Receiving Supervisor, Mentor, OH, Jan 2016- Jun 2018
Perform various tasks on the cash register, receiving cash, balancing, credit card payments, up selling, and meeting sales quotas.
Uses logic to sort, stack, unload and ship textiles.
Learning new ways of using new technologies to aide in the pricing/organization of textiles.
Multi-tasking while being flexible enough to move to different departments during busy weeks. 


I/R Technician, Spectrum, Mentor, OH, April 2014 - December 2015.
Installing new high-tech equipment in customers' homes, maintaining and troubleshooting existing systems, providing specific details pertaining to customer queries, and keeping a professional manner.
Providing customers with peace of mind, knowing their issue was solved.
Making difficult decisions based on customer needs, especially in stressful situations that can impact the company and customer. 
Evaluated trouble calls to determine the cause of the problem and most feasible solution, then chose a course of action most appropriate to the situation. 
Learned many new sales tactics, by visiting customers homes and testing out my own methods one on one, selling upgrades, and even new full installs, top on my tech team for sales for many months.
