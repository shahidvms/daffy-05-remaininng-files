

Cherie M Lumbard

58 Middlesex Street Haverhill, Ma 01835 1-508-530-2886
Email: Cheriemlum@gmail.com

Professional Summary    

I am motivated, hard working , aggressive and I lead by example.
I’m very reliable and always wanting to learn more and to be more effective and efficient. I am organized, dedicated, fast learner, self sufficient and honest. I make it my goal to make sure my staff is properly trained in any aspect of the work criteria. I’m stern but very welcoming. I take my job seriously and with pride. I’m very goal oriented.

Skills

•       Time management
•       Monitoring
•       Food production
•       Efficient and effective
•       Customer service
•       Judgment and decision making
•       Instructing
•       Strong and proven leader
•       Organized
•       Management skills


Experience
RGM 04/2017-09/21-2020

AGM, 08/2016 to 04/2017 

Shift lead 09/2015-08/2016

Tacobell Pizza Hutt - Lawrence, MA

•       Resolving customer complaints regarding sales and service
•       Plan and directing staffing, Training, and performance evaluations to develop and control sales and service programs
•       Complete order reciepts
•       Perform payroll functions and maintaining time cards
•       Operate office equipment, such as fax, copier and printer along with arranging repairs when there are system malfunctions
•       Inventory
•       Deploying staff
•       Great customer service
•       Great speed of service
•       All cash activities, making safe drops, counting cash draw
Shift Lead, 06/2015 to 08/2016
Tacobell- Haverhill, MA 01831
•       Running shifts effectively and efficiently
•       Organized
•       Counting safe and cash draws
•       Making cash drops
•       Inventory
•       Deploying staff
•       Making times and VOC to best of ability
•       Dropping prep and stock inventory
•       End of day
•       TAS
•       Learning Zone
•       Payroll and timecards are accurate
•       Sales
•       Great customer service
Crew Member, 04/2011 to 04/12
Tacobell / KFC - South Paris, ME
•       Great customer service and Friendliness
•       Cleanliness
•       Stock inventory and count inventory
•       Prepping food for both brands
•       Training new staff