LaTonya Wells
Pediatric Dental Assistant
Balch Springs, TX 75180
lwells43@yahoo.com
+1 214 434 6997
Registered dental assistant with over thirteen years of experience that is self-motivated and
dependable while achieving high performance with minimal supervision.

Work Experience
Dental Assistant

Grin Pediatric Dentistry - Plano, TX
May 2021 to October 2021
Patient educator
Coronal polishing ability
Pit and Fissure placement capabilities
Computer charting (Mac Practice)
Chat communications (Slack)
Dental assisting talent
Proficient in impression taking, production of night/sports guards, etc.
Educate patients in preventive dental care, causes and treatment of dental problems and oral
healthcare services.
Accurately collect and record patient medical and dental histories.
Recommend products and devices for patient s home care.
Obtain dental x-rays using digital methods.
Monitor patient while on nitrous.
Expertly manage difficult or emotional patient situations and respond promptly to patient s needs.
Follow dentist directions when administering anesthetics and other desensitizing agents.
Review status of waiting room on a routine basis to ensure that patients were being seen in a timely
manner.

Customer Service Representative (Remote)
Sitel Group - Miami, FL
October 2020 to April 2021

Help customers with open enrollment of healthcare benefits.
Add/delete benefit options to update customer s preferences.
(Call center) taking inbound calls
Navigate through multiple computer software.
Accurately record customer s information.

Smile Guide (Sales)

Smile Direct Club - Plano, TX
October 2019 to July 2020

� Provided great customer service while informing customers on why Smile Direct Club's clear/night time
aligners are more affordable than any other comparable brands on the market.
Built rapport with customer.
Produced customer s teeth scan using the Itero.
Recorded patient s health/medical history.
Submitted oral scan and photos to orthodontist.

Dental Assistant

Children s Dentistry of North Dallas - Dallas, TX
February 2015 to June 2019
Stocked and prepared exam and treatment rooms, setting up required instruments, tools, and
equipment.
Escorted patients to exam rooms.
Followed dentists directions when administering anesthetics and desensitizing agents.
Obtained dental x-rays using traditional and digital methods.
Passed instruments, suctioned fluids and mixed materials for impressions.
Produced opposing models, study casts, and impressions.
Took patient's blood pressure, pulse, and temperature and recorded measurements in patient s charts.
Positioned patients for treatment and prepared necessary equipment for procedures.
Educated patients regarding preventive dental care, plaque control, causes and treatment of dental
problems and oral healthcare services.
Effectively operated x-ray machines and developed x-rays.
Properly sterilized dental equipment and examination rooms in accordance with infection control
policies.
Consistently ensured the functioning and sterilization of equipment being used.
Accurately collected and recorded patient medical and dental histories.
Reviewed status of waiting room on a routine basis to ensure that patients were being seen in a timely
fashion.

Dental Assistant

Children's Dental Care - Sunnyvale, TX
October 2007 to February 2015
Stocked and prepared exam and treatment rooms, setting up required instruments, tools, and
equipment.
Escorted patients to exam rooms.
Followed dentist directions when administering anesthetics and desensitizing agents.
Obtained dental x-rays using traditional and digital methods.
Passed instruments, suctioned fluids and mixed materials for impressions.
Produced opposing models, study casts, and impressions.
Took patient's blood pressure, pulse, and temperature then recorded measurements in patient s
charts.
Positioned patients for treatment and prepared necessary equipment for procedures.
Educated patients regarding preventive dental care, plaque control, causes and treatment of dental
problems and oral healthcare services.
Effectively operated x-ray machines and developed x-rays.
Properly sterilized dental equipment and examination rooms in accordance with infection control
policies.
Consistently ensured the functioning and sterilization of equipment being used.

� Accurately collected and recorded patient medical and dental histories.
Reviewed status of waiting room on a routine basis to ensure that patients were being seen in a timely
fashion.
Charted conditions of decay and disease to prepare for diagnosis and treatment by dentist.
Expertly managed difficult or emotional patient situations and responded promptly to patient needs.
Polished and applied sealants and fluorides.
Delivered preoperative and postoperative care.
Monitored patients while on nitrous.
Consistently followed protocols regarding quality assurance, biohazards, infection control, charting
and emergencies.
Routinely completed inventory, supply orders and restocked supplies.
Recommended products and devices for patient s home care.

Education
Technical education in Dental Assisting
ATI Career Training Center-Dallas - Dallas, TX
October 2006 to October 2007

High School Diploma in Early Childhood Education and Development
Education And Social Services Magnet - Dallas, TX
August 1991 to June 1995

High School Diploma in Health Education
High School for Health Professions - Dallas, TX
August 1991 to June 1995

Skills
Chairside Assisting
Infection Control Training
Skilled in patient settings
Sealants (SE)
Study model pour and trim
Pedo and prophylaxis expertise
Restoration skilled
Application and removal of space maintainers
Cleaning of Non-digital x-ray processor
Computer charting (Dentrix Software)
HIPAA
Experience in oral and IV sedation
Pulpotomies/Porcelain ceramic and Stainless Steel crowns
Topical fluoride (TF) application
Teams and Slack communication apps

� Outlook, Excel, Microsoft Word
Zoom, WebEx,Citrix, U2X
MacPractice, Sharepoint, Adobe meetings
Customer service (10+ years)

Assessments
Call center customer service Proficient
October 2021

Demonstrating customer service skills in a call center setting
Full results: Proficient

Typing Completed
October 2021

Transcribing text
Full results: Completed

Advanced attention to detail Completed
October 2021

Identifying differences in materials, following instructions, and detecting details among distracting
information
Full results: Completed
Indeed Assessments provides skills tests that are not indicative of a license or certification, or continued
development in any professional field.

�

-----END OF RESUME-----

Resume Title: Customer Service

Name: LaTonya Wells
Email: lwells43@yahoo.com
Phone: true
Location: Balch Springs-TX-United States-75180

Work History

Job Title: Dental Assistant 05/01/2021 - 10/31/2021
Company Name: Grin Pediatric Dentistry

Job Title: Customer Service Representative 10/01/2020 - 04/30/2021
Company Name: Sitel Group

Job Title: Smile Guide (Sales) 10/01/2019 - 07/31/2020
Company Name: Smile Direct Club

Job Title: Dental Assistant 02/01/2015 - 06/30/2019
Company Name: Children s Dentistry of North Dallas

Job Title: Dental Assistant 10/01/2007 - 02/01/2015
Company Name: Children's Dental Care

Education

School: ATI Career Training Center-Dallas, Major:
Degree: None, Graduation Date:

School: Education And Social Services Magnet, Major:
Degree: High School, Graduation Date:

School: High School for Health Professions, Major: Not Applicable
Degree: High School, Graduation Date:

Additional Skills And Qualifications

Most Recent Job Title: true
Most Recent Pay: USD per
Managed Others: true
Languages Spoken: English
Work Authorization: Not Specified
Security Clearance: No
Felony Conviction: No

Desired Pay: USD per
Desired Job Types: Full Time
Travel Preference:

Currently Employed: No
Jobs Last Three Years: 0
Last Job Tenure Months: 0
Highest Degree: High School Degree
Certifications:
Employment Type:
Military Experience:
Last Updated: 10/26/2021 12:00:00 AM

Desired Shift Preferences:

Interests:
Health Care

Downloaded Resume from CareerBuilder
RDD63579JSC098VFXHQ


