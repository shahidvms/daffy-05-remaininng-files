Gabriel Giersdorf
205 Cedar Ln, Washington, NC 27889
ggiersdo@gmail.com
QUALIFICATION SUMMARY:
Over 21 years of experience as Maintenance Planner and Repair Planner in the Sawmill Lumber, Food Manufacturing, Pharmaceutical Manufacturing and military environments. Maintenance Planner at Weyerhaeuser Lumber Sawmill, Planner and Scheduler at the Starbucks Carson Valley Roasting Plant; Facility Maintenance Planner at Amgen, Inc.; Submarine Electrical Repair Planner at the SIMA San Diego Naval Base.  10 1/2 years experience as Submarine Nuclear Electrician's and Power Plant Operator.  Advanced user of SAP, MP2 and Maximo CMMS software as well as all Office business applications.  Expert at report design and use of Excel, Hyperion Intelligence (Brio Query) software and proficient with Crystal Reports.

AREAS OF EXPERTISE:
� SAP (CMMS), Maximo (CMMS) Adv. User � MP2 (CMMS) Adv. User
� Chain and Belt Conveyor Systems, Gangsaw, Primary Breakdown, Sawmill Residuals
� Hydraulic Systems � Pneumatic Systems � Lubrication
� Hyperion Intelligence Report Development, � Data Mining � Data Analysis
� Maintenance Planning & Scheduling � Maintenance Work Flow
� Technical Research � Work Order (Procedural) Development � Plant Operations
� Engineering � Quality Assurance � 21 CFR Parts 210 and 211 � cGMP
� Electrical Maintenance � Repair Parts Acquisition � Blueprint Interpretation � Training
� Network Administration � Microsoft Office Software � Electronic Form Creation

EXPERIENCE:
January 2013 to Current
Green lumber maintenance planner for all work order activities at the sawmill. Utilizing SAP and formerly Maximo for all planning and scheduling activities and repair parts acquisitions to support the sawmill production.  Weekend supervisor for all maintenance activities.

Feb 2010 to November 2012 - Maintenance Planner/Scheduler
Maintenance Department at the Starbucks Coffee Company Carson Valley Roasting Plant
Sole planner and scheduler for all maintenance work order activities at Starbucks roasting plant using Infor (DataStream) MP2. Plan all standard and shutdown maintenance and contractor work in a rapidly changing production environment. Coordinate all non-capital contractor activities in roasting plant in support of the maintenance and facility maintenance departments. Create and update all maintenance procedures in MP2 CMMS. Manage work orders from creation to closing in MP2 CMMS. Solely responsible for coordination of all maintenance activities with production scheduling to ensure successful sustained operation of plant manufacturing equipment. Procedural and Work Flow Development for CMMS.

Oct 2007 to Feb 2010 � In Transition (Full time student)

Oct 2003 to Oct 2007 - Maintenance Planner
Facilities Business Operations at Amgen Thousand Oaks
Work Order Development in cGMP Manufacturing and Administrative Facilities Projects including building reconfigurations and lab equipment & instrumentation moves, calibrations and recertification. Work Order Planning for General, Corrective and Preventative maintenance for the Instrumentation, Automation and Certification crafts for the Amgen Thousand Oaks site. Key member in the creation and updating of thousands of maintenance job plans to support the upgrade from Maximo 4.1 to version 5.2 for Thousand Oaks, CA and Louisville, KY. Automated the data-entry process saving thousands of non-value added man-hours during the update process. Operated as the sole non-manufacturing maintenance scheduler for Amgen Thousand Oaks (>40 lab and administrative buildings) for most of 2004 and all of 2005, which also involved the coordination between crafts, internal clients and outside vendors in the performance of maintenance to Amgen equipment.
Updated hundreds of thousands of asset records in the Amgen Computerized Maintenance Management System (CMMS) through 2004 and 2005 to support maintaining highly accurate system, equipment and maintenance records. Solely responsible for non-manufacturing PM generation, PM report creation and distribution through 2004 and 2005 for Amgen Thousand Oaks and Amgen Louisville. Solely responsible for the scheduling and coordination of all preventative maintenance work orders with the potential to affect IS/IT equipment between the maintenance crafts and the IS Change Control Board (CCB/CAB) ensuring a high state of equipment readiness with minimal risk to company electronic data and intellectual property. Developed and maintained the site procedures used for the non-manufacturing PM generation, scheduling and report creation. Developed dozens of Brio Query (Hyperion Intelligence) database reports for asset management research and record audits used by the Operations, Planning & Scheduling staff as well as key clients throughout the company and the FDA.
Created dynamic business forms used by the Amgen Facilities Business Operations unit saving time and freeing resources in the Document Management group. 

2000 to 2003 - Naval Electrical Repair Planner
San Diego Submarine Maintenance Division Shore Intermediate Maintenance Facility 
Independently determined work requirements, estimates for labor, needed repair materials for submarine electrical and mechanical repairs to Sonar Systems, Communication Antennas, and Electrical Motors and wire ways. Estimate time requirements for the preparation of work item requirements for the support of repairs to small boats and submarines. Researched technical drawings to determine correct material needed and order any required repair materials from government and local civilian manufacturing agencies and suppliers. Trained new repair planners in the development of Quality Assurance Repair Procedures. Maintained and Administered the Advanced Technical Information System (ATIS) computer network used to provide all drawings and technical manuals for the research of Repair Procedures.

1995 to 2000 - Naval Nuclear Electrical Operator and Shutdown Reactor Operator
Electrical Division, USS Pasadena (SSN 752), Pearl Harbor, HI
Supervised and Operated the Shut-Down Reactor Plant and Electrical Distribution System through several periods of Special Testing and widely changing reactor plant operations. Operated and maintained the submarine electrical distribution system.  Performed Preventative and Corrective Maintenance on all submarine electrical equipment including all Electrical Distribution Controllers, Propulsion Plant Motors and the Lighting System.  Researched and ordered all required repair and maintenance materials as the Electrical Division Repair Parts Petty Officer. Acted as the Electrical Division Sound Analysis Petty Officer obtaining sound cuts on the submarine hull and motors to support the reliability based maintenance.

EDUCATION:
2009 to 2011 Student at Grantham University working towards BS in Computer Science
2007 Attended classes at University of Phoenix towards BS in Business/Information Systems
Nov 05 Marshall Institute Seminar for Effective Planning and Scheduling
Jul 04 Effective Business Writing (Amgen)
Jun 04 Qualified as member of Amgen Emergency Response Team
1994 Naval Nuclear Power Training Unit, Idaho Falls, ID
1994 Naval Nuclear Power School, Naval Training Center, Orlando, FL
1993 Electrician�s Mate Trade School, Naval Training Center, Orlando, FL
1992 El Camino College, Torrance, CA � Physics Major
1991 Graduated from Narbonne High School, Harbor City, CA 

LEADERSHIP:
Supervise and responsibility for weekend and holiday maintenance activities at Weyerhaeuser sawmill while mills are down to support PM accomplishment, projects and repairs. 
Demonstrated and trained Amgen Planning & Scheduling group on use of Brio queries and Excel VBA data loaders to save thousands of man-hours in identifying work requirements and updating the CMMS.
Qualified Shut Down Reactor Operator - Supervised and directed the operations of six subordinates and the safe operation of all electrical and mechanical systems associated with the reactor plant and engineering spaces during operating and special testing conditions. 

COMPUTER SOFTWARE:
Experienced with SAP, MP2 and Maximo, all versions of Microsoft Windows and Office Software. Specifically, I have extensive experience with Excel, Word and Outlook and am familiar with Access, PowerPoint, Visio and Project. I am also very experienced with Internet Browsers, Professional versions of Adobe Acrobat and Adobe LiveCycle Designer for dynamic form creation. I am experienced with Ad Hoc Database Query Creation using Hyperion Intelligence (Formerly Brio and now part of Oracle Business Intelligence) and Crystal Reports.

TRAINING:
Certified as Forklift Operator at Weyerhaeuser Lumber.
Attended Reliability Solutions Essential Craft Skills I and II classes while at Weyerhaeuser Lumber.
Attended GPM Hydraulics Reliability and Troubleshooting Course while at Weyerhaeuser Lumber.
As a Shipboard Instructor, I trained junior officers and enlisted personnel in plant supervision, operations, and maintenance in both class room and shipboard environments. I have generated many training topics using Microsoft PowerPoint for use in the Navy and at Amgen, Inc
