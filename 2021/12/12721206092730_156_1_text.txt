﻿MICHAEL L. VASSER
8032 S. Exchange, Chicago, IL 60617
(708) 383 – 5210 * mvasser2020@mchs.org
Qualifications
Deeply responsible, dependable, hard-working, enthusiastic individual who is positioned to excel, enjoy working with people, and wants to be a part of a winning team.
• Communication and Teamwork: Engaging interpersonal skills in working with fellow students in group project problem solving and team participation.
• Additional Skills: Quick learner, proactively observing processes to swiftly gain mastery of new skills and techniques. Technical proficiencies include MS Office Suite and social media.
Education
Thornton Fractional North High School
755 Pulaski Rd, Calumet City
3.5 GPA

Experience Highlights
Mount Carmel High School, Chicago, IL
Kitchen Aide Volunteer, September 2017 to June 2018
• Duties performed as a cafeteria aid on a weekly basis: Replenished food items on display for students & staff during assigned lunch period, wash dishes, clean cafeteria area, and count various kitchen inventory.
TNT Barbershop, Chicago, IL
Barbershop Assistant, May 2019
• Greet all customers as they enter the shop and direct them to the guest waiting area.
• Scrubbing and sanitizing restrooms, cleaning all service areas, and maintaining floors.
• Answer phones, direct calls, and take messages as needed.

