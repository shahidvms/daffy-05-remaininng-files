 Shawnta Davis
303 McNary Ave. Canonsburg, PA 15317 (724)206-6513

OBJECTIVE: 
•Seeking a position in which my skills, background and knowledge can be used. Professional Skills
• Ability to organize, multi-task, and manage time in an Administrative capacity.
• Proficient in Microsoft Word, Excel, PowerPoint, Google Drive, Google Cloud, and Email.
• Good filing and organizational skills.
• Experience in handling confidential paperwork.
• Ability to take accurate phone messages and deliver messages promptly.
• Phlebotomy Experience and immunization.
• Experienced in ICD-9 and CPT coding, and insurance form completion.
• Worked with Medisoft Medical Software.
• Good customer-service background. 

EMPLOYMENT HISTORY

Sarris Candies  Warehouse 2019-2020

	•	Warehouse worker
	•	Boxed candies to have shipped
	•	Created shipping labels
	•	Made chocolate candies, and boxed them
	•	Prepared fund raisers 

Formative Health 2016-2019 Payment Posting Specialist Canonsburg, Pa

	•	Post insurance and patient payments for multiple physician dme office’s.
	•	Review eob’s
	•	Insurance/Patient refunds
	•	Print claims to be mailed
	•	Email/Fax
	•	Request paid eob’s from insurances if needed
	•	Work from home independently

Select Medical 2014-2015 A/R Billing Specialist
Southpointe, Pa

	•	Physical Therapy Billing/Collections 
	•	Verifying insurances/ Working denied claims
	•	Following up on claims 
	•	Incoming/Outgoing calls, Faxing to physician offices and attorneys

Apria Healthcare 2011-2014 Medicare A/R Billing Specialist Southpointe, PA

	•	Durable Medical Billing/Collections
	•	 Faxes/Answering phone and outbound calls
	•	Working denied claims for medicare insurance
	•	Appeals
	•	Worked from home

CALL CENTER 2011-2011
NCO, Canonsburg, PA

	•	Answering inbound calls about electric bills.
	•	Taking payments via phone.
	•	Assisting customers with electric billing inquires and referring them to other agencies.

MEDICAL OFFICE 2009-2010
Adult Neurology Center, Washington, PA

	•	Accurately put patient information in computers.
	•	Answering phones, and scheduling appointments.
	•	Faxing paperwork, and mailing.
	•	 Verifying patients insurance.

CASHIER 2000-2007
Wendy’s, Canonsburg, PA

	•	Took orders, served restaurant patrons, and assisted at the cash register.
	•	Provided excellent customer service to all customers.
	•	Assisted with scheduling.
	•	Assisted with preparing food, clean dining room. 

EDUCATION
Penn Commercial, Washington, PA
	•	Associates degree in Medical Assistant

Canon-Mcmillian, Canonsburg, PA 
	•	High School Diploma
