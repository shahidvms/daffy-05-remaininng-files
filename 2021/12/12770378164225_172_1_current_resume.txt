Sharon  Monteilh
571-278-1499 | smonteilh@yahoo.com | 1215 Shore Dr. SW, Atlanta Ga 30311



Professional Summary
An extensive number of years of Customer Service experienced interfacing with the public on phone and in person. Capable of providing excellent customer service experience. Knowledgeable of products offered, able exceed customer expectations with attention to details.

Skills
Multi-Tasking             Communication
Organization              Critical Thinking
Research and analysis

Experience
Retired At&T, Atlanta GA            10/1979-3/2018
Communication Technician,
  Provision t1.5 circuits, testing and turning up to customers, performing changes in existing service. working with internal and external customers to resolve issues.
Circuit Designer
  Designed low-speed circuits provisioned facilities/equipment necessary to provide service for customers.
Billing Clerk
  Managed Dedicated Large Business/Government Customer accounts, Services, credit adjustments, collections.

Education
Certification A+ Net+
Austin Community College General Studies 21 cr hrs
Parkland High School El Paso Tx, Graduate 1977