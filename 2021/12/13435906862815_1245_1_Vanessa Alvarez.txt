Summary

Skills

Experience

Education and Training

Languages

Vanessa Alvarez
Los Angeles, CA 90002

Cell: 323-535-5145

vanessaalvarez002@yahoo.com

Caring Case Manager experienced in coordinating care for Homeless Veteran individuals. Proactive and resourceful
professional

able to make appropriate individual career planning services. Careful provider of preventative and proactive care for
vulnerable

individuals.

Microsoft Office

HIPPA Compliance

Client intake

Data Entry

Claims processing

Case Management

Volunteers of America of Los Angeles - Work Study

May 2020 to Current

Intake Specialist/Case Manager Intern

Compton, CA

Input assessments, individual employment plans (IEP), and updated Case Notes into the secure internal VOALA
database
system with accuracy while verifying data integrity.

Single-handedly closed out 3 Fiscal Years of client files for the Homeless Female Veterans & Veterans with Families

2017, 2018, & 2019 by exiting on database, alphabetically filing, and preparing for storage.

Maintain Purchase Orders for the Program Manager by updating log and keeping note on spending purchases.

Determined whether Veteran clients should be counseled or referred to other specialists via telephone assessments.

Conducted interviews with participants, explaining benefits process and which programs were available based on

determined eligibility.

Explained vocational and technical career training support for career pathways to Veteran participants.

Altamed Health Services

March 2007 to May 2016

Medical Assistant II

Los Angeles, CA

Documented intake into Electronic Health Record internal database for organization. Keeping Medical Case Notes,

Treatment Plans accurate and daily updated.

Maintained patient confidentiality by upholding HIPPA laws at all times.

Provided referrals for specialty appointments and logged case notes for follow-up treatment in various database systems.

Executed a training curriculum to new and existing Medical Assistant Staff to enhance the performance and service

delivery.

Maintained industry updated information by participating in monthly staff development training such as: policies,
process

changes, andn various medical updates.

Centinela Hospital - Urgent Care

October 2001 to March 2006

Medical Assistant

Los Angeles, CA

Performed all aspects of patient intake, such as vitals and chief complaints to document on patient chart.

Case managed files for workers compensation, insurance adjusters, drug screen collection, pre-employment physicals,

executive physicals, FAA physicals, and maintained audit readiness on all hard files.

Maintained patient care areas clean and stocked and assisting with supplies, medications, vaccines inventory, and logs.
Scheduled follow-up appointments and 'no-shows' into database system for follow-up and retention.

Functioned as a patient advocate by ensuring patient concerns were resolved and/or addressed in a timely manner.

Performed all requested orders such as vaccines, medication administration, labs, CLIA Waive Test, Hearing and Vision
in

order to prepare for provider visit.

United Education Institute

1997

Diploma

:

Medical Assistant

Jordan High School

1993

High School Diploma

Los Angeles, CA

Bilingual - fluent in English/Spanish

Powered by TCPDF (www.tcpdf.org)

-----END OF RESUME-----

Resume Title: Intake Specialist Case Manager Intern

Name: Vanessa Alvarez
Email: vanessaalvarez002@yahoo.com
Phone: (323) 535-5145
Location: Los Angeles-CA-United States-90002

Work History

Job Title: Intake Specialist/Case Manager Intern 05/01/2020 - Present
Company Name: Volunteers of America of Los Angeles

Job Title: Medical Assistant II 03/01/2007 - 05/31/2016
Company Name: Altamed Health Services

Job Title: Medical Assistant 10/01/2001 - 03/31/2006
Company Name: Centinela Hospital - Urgent Care

Education

School: Jordan High School, Major: Not Applicable
Degree: High School, Graduation Date:

School: true, Major:
Degree: High School, Graduation Date:

Additional Skills And Qualifications

Most Recent Job Title: true
Most Recent Pay: USD per
Managed Others: true
Languages Spoken:
Work Authorization: Not Specified
Security Clearance: No
Felony Conviction: No

Desired Pay: USD per
Desired Job Types: ["Full Time","Part Time","Intern","Seasonal","Temporary","Contractor"]
Travel Preference:

Currently Employed: No
Jobs Last Three Years: 0
Last Job Tenure Months: 0
Highest Degree: High School Degree
Certifications:
Employment Type:
Military Experience:
Last Updated: 10/28/2021 8:36:51 PM

Desired Shift Preferences:

Interests:
Nonprofit - Social Services
NGO-Social Services
Volunteering

Downloaded Resume from CareerBuilder
R2R4YH7296TQ9HWQBPN


