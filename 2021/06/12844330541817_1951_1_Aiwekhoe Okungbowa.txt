Aiwekhoe Okungbowa
Long Beach, CA 90802 916-247-8014
aiweokungbowa@gmail.com

PROFESSIONAL Certified Scrum Master with 4+ years of experience working with Agile and
SUMMARY Scrum teams. A dedicated team player with strong critical thinking and decision-
making skills committed to executing project goals in the most effective and time-
efficient manner using Lean Agile principles and values. Self-motivated individual
with the ability to work independently, meet tight deadlines, thrive under
pressure, foster team collaboration, and resolve conflicts within teams. Career
Highlights: Project Management and Scrum Master experience utilizing Waterfall
and Agile methodologies.

SKILLS Visual Studio, Jira, Rally, TFS, and Promoting continuous
MS Project. improvements and helping teams
Knowledge in Agile transformation increase productivity to foster
and Agile sustainment for both innovation
experienced and new Agile teams. Risk identification
Proficient in Microsoft Office Detail-oriented
(Word, Excel, Outlook,
PowerPoint, Visio).
Ability to engage and motivate
teams toward a common goal

WORK HISTORY SCRUM MASTER 06/2019 to CURRENT
Farmers Insurance | Woodland Hills, CA
Facilitate Scrum ceremonies such as: Sprint Planning, Daily Standup,
Sprint Reviews, Retrospectives, and others to build and maintain high-
performing teams.
Aid team and Product Owner in efforts to manage and sequence the
backlog.
Supports Product Owners in maintaining the product backlog, roadmap,
and delivery deadlines, breaking down high-level business intents into
workable components to achieve an efficiently prioritized high-value
backlog.
Assists in building a high-performing team and promote relentless
improvement at the Team level while improving team performance in areas
of quality, predictability, flow, and velocity.
Utilizes intermediate coaching capabilities to influence teams and help
coach on practices and procedures, soft skills, and Lean-Agile mindset.
With occasional support, guides teams through the decision-making
process and teaches techniques on how to problem solve, improve
communication, self-manage and self-organize.
Supports a trusting and safe environment where problems can be raised.
Helps manage interpersonal conflicts, challenges and opportunities for
growth within the team.
Proactively manage risks, assumptions, issues, dependencies, and
ensuring the coordination to escalate impediment rapidly.
Protects teams from interruptions and removes impediments that may
impact the delivery of the product.
Continuously coaches team members on the application of Agile principles
to daily activities to ensure smooth functioning of the delivery team.
Utilizes team feedback and team metrics to identify areas of opportunity
and works with the team to continuously improve.
Provided extensive guidance on Agile scrum processes and methodologies
to highly effective teams with goals of improving quality and productivity.
Coached teams in Agile practices and provided necessary training to create
a positive mindset to Agile methodologies.
Investigated and corrected or escalated project problems.

PROJECT MANAGER / MANAGEMENT ANALYST 04/2016 to 05/2019
Department of Health Services | Los Angeles, CA
Assist in designing and piloting the transition to online competency testing
for DHS.
Provides technical assistance to Chiefs and Managers for the online
competency testing project.
Assist testers and proctors as well as resolving technical issues during
online competency testing.
Answer technical questions via email and telephone to DHS employees
and managers.
Analyzes and resolves issues and concerns of DHS staff.
Worked effectively with cross-functional design teams to create software
solutions that elevated client experience and significantly improved overall
functionality and performance.
Acts as a liaison between various teams on special projects.
Member of the Audit team.
Managed different projects by planning, coordinating, monitoring and
supporting the implementation of multiple small to medium systems and/or
products.
Assists in Joint Commission & State visits at County Medical Center.
Tasked with leading special projects geared towards improving
departmental effectiveness & productivity including marketing, analyzing &
conducting research.
Communicated with subject matter experts on the various components
relating to the project and its components.
Assists in conducting studies to ascertain objectively, the efficiency and
effectiveness of departmental operations, policies and procedures.
Optimized system and platform performance with periodic system updates.
Reviewed internal systems and organized training plan to address areas in
need of improvement.

SCRUM MASTER 04/2015 to 04/2016
Albertsons Companies | Pleasanton, CA
A Servant Leader that earns the teams respect and provides hands-on
assistance.
Ensured the development team practiced agile principles of collaboration,
prioritization, team accountability, and visibility to remain high-performing.
Established timely and consistent collaboration and coordination between
analysts, developers, and testers within the team to ensure the
achievement of each project/sprint goal.
Worked closely with Product Owners and Stakeholders to refine the vision
and establish goals and metrics.
Tracked and communicated team velocity and sprint progress to all
affected teams and management.
Facilitates discussions that lead to the resolution of project issues and
development impediments.
Ensures all proper training and user documentation has been implemented.
Communicates openly and frequently with stakeholders and subject matter
experts regarding the state of the project.

EDUCATION Project Management
Six Sigma Global Institute

Master of Science | Health Care Management 05/2016
California State University, Los Angeles, CA

Bachelor of Science | Public Health 05/2014
California State University, Los Angeles, CA

Scrum Master (SSM)
Scaled Agile

.

-----END OF RESUME-----

Resume Title: Aid Team Product Owner

Name: Aiwekhoe Okungbowa
Email: aiweokungbowa@yahoo.com
Phone: (916) 247-8014
Location: Long Beach-CA-United States-90802

Work History

Job Title: SCRUM MASTER 06/01/2019 - Present
Company Name: Farmers Insurance

Job Title: PROJECT MANAGER / MANAGEMENT ANALYST 04/01/2016 - 05/31/2019
Company Name: Department of Health Services

Job Title: SCRUM MASTER 04/01/2015 - 04/01/2016
Company Name: Albertsons Companies

Education

School: Six Sigma Global Institute, Major:
Degree: None, Graduation Date:

School: California State University, Major:
Degree: Master's Degree, Graduation Date:

School: California State University, Major:
Degree: Bachelor's Degree, Graduation Date:

Additional Skills And Qualifications

Most Recent Job Title: true
Most Recent Pay: USD per
Managed Others: true
Languages Spoken:
Work Authorization: Not Specified
Security Clearance: No
Felony Conviction: No

Desired Pay: USD per
Desired Job Types: ["Full Time","Part Time","Intern","Seasonal","Temporary","Contractor"]
Travel Preference:

Currently Employed: No
Jobs Last Three Years: 0
Last Job Tenure Months: 0
Highest Degree: 4 Year Degree
Certifications:
Employment Type:
Military Experience:
Last Updated: 5/25/2021 4:49:37 AM

Desired Shift Preferences:

Interests:
Information Technology
Internet-E-Comerce

Downloaded Resume from CareerBuilder
R2R7N86ZBY3HM71YQ53


