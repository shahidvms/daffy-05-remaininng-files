Larry D Willis
2305 Village cir. Apt 214
Bedford, TX 76022
Phone: (318) 663-2655
Alt Phone:(318) 652-2655
Email: larrywillisjr@hotmail.com

Objective
Forward-thinking individual with refined interpersonal and multitasking skills. Looking to join a progressive organization as a motivated dedicated employee to be an asset, gain knowledge, and grow within the company.

Abilities
	•	Experience in direct supervising and coordinating the activities of inmates.
	•	CDL and some driving experience.
	•	       • Post certified    
	•	  
	•	       • I have people person skills

	•	       • Have a hard working ethic                                               

Employment History
Owner		
02/2018 - 04/2020	Dolled up	713 Kerry st., Natchitoches, LA
operator		
		
Shift Lieutenant		
07/2011 - 12/2019	Natchitoches parish shrieff department	Natchitoches, LA
Correctional officers must follow procedures to maintain their personal safety as well as the safety of the inmates they oversee. Correctional officers are responsible for overseeing individuals who have been arrested and are awaiting trial or who have been sentenced to serve time in jail or prison. Bailiffs, also known as marshals or court officers, are law enforcement officers who maintain safety and order in courtrooms. Their duties, which vary by court, include enforcing courtroom rules, assisting judges, guarding juries, delivering court documents, and providing general security for courthouses. Duties Correctional officers typically do the following: * Enforce rules and keep order within jails or prisons * Supervise activities of inmates * Inspect facilities to ensure that they meet security and safety standards * Search inmates for contraband items * Report on inmate conduct * Escort and transport inmates Bailiffs typically do the following: * Ensure the security of the courtroom * Enforce courtroom rules * Follow court procedures * Escort judges, jurors, witnesses, and prisoners * Handle evidence and court documents Inside the prison or jail, correctional officers enforce rules and regulations. They maintain security by preventing disturbances, assaults, and escapes, and by inspecting facilities. They check cells and other areas for unsanitary conditions, contraband, signs of a security breach (such as tampering with window bars and doors), and other rule violations. Officers also inspect mail and visitors for prohibited items. They write reports and fill out daily logs detailing inmate behavior and anything else of note that occurred during their shift. Correctional officers may have to restrain inmates in handcuffs and leg irons to escort them safely to and from cells and to see authorized visitors. Officers also escort prisoners to courtrooms, medical facilities, and other destinations. Bailiffs' specific duties vary by court, but their primary duty is to maintain order and security in courts of law. They enforce courtroom procedures that protect the integrity of the legal process. For example, they ensure that attorneys and witnesses do not influence juries outside of the courtroom, and they also may isolate juries from the public in some circumstances. As a neutral party, they may handle evidence during court hearings to ensure that only permitted evidence is displayed.		
		
Correctional Officer		
08/2010 - 07/2011	Winn CCA	Winnfield, LA
Guard inmates in penal or rehabilitative institution in accordance with established regulations and procedures. May guard prisoners in transit between jail, courtroom, prison, or other point. Includes deputy sheriffs and police who spend the majority of their time guarding prisoners in correctional institutions.		
		
Maintenance and Repair Worker		
11/2008 - 08/2010	Self Employed	Natchitoches, LA
Contracted to various retail businesses on a as need basis with duties that may include but not limited to:clean windows, fixed cabinets, minor plumbing, changing light bulbs, service vacuum cleaners, inventory control and  unload/load merchandise		
		
Foreman		
09/2007 - 10/2008	Shaw Construction	Lena, LA
I supervised projects and made sure they were completed in a reasonable time frame,and also made sure that my crew keep a safe work record.I supervised as many as a 20 man crew.I aalso had a safe work recored no recordbles at all.		
		
Production		
08/2007 - 09/2007	Pilgrims Pride	Natchitoches, LA
production worker at poultry worker		
		
Linesman		
08/2006 - 09/2006	Fairfield	Lafayette, LA
Responsible for handling, deploying and retrieving of all seismic equipment and other general maintenance and upkeep of the vessel. Performs general housekeeping and upkeep of the vessel, loading and unloading crew supplies and groceries. Responsible for maintaining a clean boat deck as well.		
		
Production Worker		
07/2005 - 08/2006	Martco	Chopin, LA
Set up or set up and operate wood-sawing machines. Examine blueprints, drawings, work orders, and patterns to determine size and shape of items to be sawed, sawing machines to set up, and sequence of sawing operations.		
		
Assembler		
03/1999 - 06/2005	Alliance Compressors	Natchitoches, LA
Work as part of a team having responsibility for assembling an entire product or component of an air-conditioning compressor. Team assemblers can perform all tasks conducted by the team in the assembly process and rotate through all or most of them rather than being assigned to a specific task on a permanent basis. .		
		
Correction Officer		
02/1999 - 03/1999	Winnfield Correctional Center	Winnfield, LA
Provide social services to assist in rehabilitation of law offenders in custody or on probation or parole. Make recommendations for actions involving formulation of rehabilitation plan and treatment of offender, including conditional release and education and employment stipulations.		
		
Education History
Completion Date	Issuing Institution	Location	Degree Received	Course of Study
05/1995	Natchitoches Central High	LA	High School Diploma	High School Diploma
References Available on Request